terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.14.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "do-lon1-k8s-cms-stage"
}

resource "kubernetes_deployment" "blue" {
  metadata {
    name = "blue"
    namespace = "prometheus"
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "blue"
      }
    }

    template {
      metadata {
        labels = {
          app = "blue"
        }
      }

      spec {
        container {
          image = "hashicorp/http-echo"
          name  = "blue-app"
          args = ["-listen=:8080", "-text='I am blue'"]
          port {
            container_port = 80
            }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "green" {
  metadata {
    name = "green"
    namespace = "prometheus"
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "green"
      }
    }

    template {
      metadata {
        labels = {
          app = "green"
        }
      }

      spec {
        container {
          image = "hashicorp/http-echo"
          name  = "green-app"
          args = ["-listen=:8081", "-text='I am green'"]
          port {
            container_port = 80
            }
        }
      }
    }
  }
}

resource "kubernetes_service" "blue-svc" {
  metadata {
    name = "blue"
    namespace = "prometheus"
  }
  spec {
    selector = {
      app = "blue"
    }
#    externalIPs = "192.168.25.101"
    port {
      port        = 80
      target_port = 8080
    }

  }
}

resource "kubernetes_service" "green-svc" {
  metadata {
    name = "green"
    namespace = "prometheus"
  }
  spec {
    selector = {
      app = "green"
    }
#    externalIPs = "192.168.25.101"
    port {
      port        = 80
      target_port = 8081
    }

  }
}

resource "kubernetes_ingress_v1" "blu-ing" {
  metadata {
    name = "blue"
    namespace = "prometheus"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {


   rule {
     host = "blue-green.kreatio.info"
      http {
        path {
          backend {
            service {
              name = "blue"
              port {
                number = 80
              }
            }
          }
         }
        }
       } 
  }
}

resource "kubernetes_ingress_v1" "green-ing" {
  metadata {
    name = "green"
    namespace = "prometheus"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "nginx.ingress.kubernetes.io/canary" = "true"
      "nginx.ingress.kubernetes.io/canary-weight" = "25"
    }
  }

  spec {


   rule {
     host = "blue-green.kreatio.info"
      http {
        path {
          backend {
            service {
              name = "green"
              port {
                number = 80
              }
            }
          }
         }
        }
       } 
  }
}
